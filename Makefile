# 
# Miguel Retamozo
# 
# set DEVBASE as a env variable: export DEVBASE=<Path>

	
BIN = exchange_rate
LINKER_FLAGS = 
OBJS = main.o utils.o


IN = ${DEVBASE}/inc

all:$(BIN)

$(BIN): $(OBJS)
	gcc -g -Wall -o $(BIN) $(OBJS) $(LINKER_FLAGS)


main.o: main.c
	gcc -g -Wall -c main.c -I$(IN)

utils.o: utils.c
	gcc -g -Wall -c utils.c -I$(IN)

clean:
	rm $(OBJS) $(BIN)
