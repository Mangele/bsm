#ifndef _UTILS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define _UTILS_H
#define TMP_CURL_FILE_NAME "/tmp/tmp_curl_file.html"
#define MAX_LEN_FILE_NAME 256
#define MAX_LEN_LINE  256
 

void print_usage(char *binary);
long int find_size(char file_name[]);

#endif