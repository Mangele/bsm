#include "utils.h"

// deprecated
long int find_size(char file_name[]) 
{ 
    // opening the file in read mode 
    FILE* fp = fopen(file_name, "r"); 
  
    // checking if the file exist or not 
    if (fp == NULL) { 
        printf("File Not Found!\n"); 
        return -1; 
    } 
  
    fseek(fp, 0L, SEEK_END); 
  
    // calculating the size of the file 
    long int res = ftell(fp); 
  
    // closing the file 
    fclose(fp); 
  
    return res; 
} 

void print_usage(char *binary){

	fprintf(stderr,
		"Usage:\n"
		"%s -o <filename> -c <currency> -f <format>\n"
		"filename: output.sql\n"
		"currency: AUD,USD,SEK .. etc see more at https://www.x-rates.com/\n"
		"format: infile or query\n",binary);
	exit(EXIT_FAILURE);
}


