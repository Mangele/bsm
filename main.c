
#include "utils.h"


int main(int argc, char **argv) {

    // File pointer variables
    FILE* fp_query_file = NULL;		// File pointer query file
    FILE *p;

    char *countries[200]={};
    float pt_amount[200] = {};

    // Currency variables
    char from_cur[4]={0};
    char to_cur[4]={0};				
    float exchange_rate;			
    float amount = 0;

    // Local variables
    char *string = NULL;
    char *found = NULL;
    char *query_file_name = NULL;
    char *line_buffer = NULL;
    char format[7]={0};
    int option;
    char cmd[150] = {0};

    if (argc < 2)
        print_usage(argv[0]);
    // Parsing arguments
    while((option = getopt(argc, argv, "o:c:a:f:h")) != -1){ //get option from the getopt() method
        switch(option){
            //For option i, r, l, print that these are options
            case 'o': // output file
                if (strlen(optarg) > MAX_LEN_FILE_NAME){
                    fprintf(stderr, "ERROR: file name is too long it has [%ld] characters, it should has less than [%d]\n", 
                            strlen(optarg), MAX_LEN_FILE_NAME);
                    print_usage(argv[0]);
                }
                query_file_name = (char *)malloc(sizeof(char)*MAX_LEN_FILE_NAME);
                strncpy(query_file_name, optarg, strlen(optarg));
                break;
            case 'c': // from currency
                if ( strlen(optarg) != 3){
                    fprintf(stderr, "ERROR: your currency has [%ld] characters, it should has 3", strlen(optarg));
                    print_usage(argv[0]);
                }
                strncpy(from_cur, optarg, strlen("AUD"));
                break;
            case 'a': // Amount
                //amount = atof(optarg); TODO: undo comment if yuo wan to add amoiunt as parameter
                break;
            case 'f': // format
                if (strncmp(optarg, "query", 5) != 0 && strncmp(optarg, "infile", 6) != 0){
                    fprintf(stderr, "ERROR: [%s] format is not valid, choose among <query|infile>\n", optarg);
                    print_usage(argv[0]);
                }  		
                strncpy(format, optarg, strlen(optarg));
                break;
            case 'h':
                print_usage(argv[0]);
                break;
            default: print_usage(argv[0]); 

        }
    }

    amount=1; //TODO remove if you want to add amount as parameter

    // forming cmd
    snprintf(cmd,sizeof(cmd)-1,"/usr/bin/curl -s \'https://www.x-rates.com/table/?from=%s&amount=%f\'"
            " |grep \"td class\" | tail -n +21 ",from_cur, amount);

    p = popen(cmd,"r");
    if( p == NULL)
    {
        print_usage(argv[0]);
        return -1;
    }

    //Assign memory to read file line by line
    line_buffer = (char *)malloc(MAX_LEN_LINE * sizeof(char)+1);
    if(!line_buffer){
        fprintf(stderr, "ERROR: In [%s] no memory allocated", line_buffer);
        return -2;
    }


    fp_query_file = fopen(query_file_name,"w+");
    if(fp_query_file == NULL)
        return -3;

    int line = 0;
    // go through open buffer line by line
    while( fgets(line_buffer, MAX_LEN_LINE, p)){

        // Parsing parameters
        string = strdup(line_buffer);
        found = strsep(&string,"?");
        found = strsep(&string,"?");
        sscanf(found,"from=%3c&amp;to=%3c\'>%f</a></td>",from_cur, to_cur, &exchange_rate);

        // Write to file
        fprintf(fp_query_file,"%s,%s,%7.8f\n",
                from_cur,to_cur,exchange_rate);

        // Allocating/Assigning dynamic memory
        countries[line] =(char *)malloc(4*sizeof(char));
        strncpy(countries[line], from_cur,3);
        pt_amount[line] = exchange_rate;

        int i =0;
        // Algorithm to deduce the rest of rate exchange
        while( line > 2 && line & 1 && i < (line-1)/2  )
        {
            if ( !strncmp(format, "infile",strlen("infile"))){
                fprintf(fp_query_file,"%s,%s,%7.8f\n",
                        countries[line],countries[2*i+1], pt_amount[2*i]/pt_amount[line-1]);

                fprintf(fp_query_file,"%s,%s,%7.8f\n",
                        countries[2*i+1], countries[line], pt_amount[line-1]/pt_amount[2*i]);

            }else if ( !strncmp(format, "query",strlen("query"))){
                fprintf(fp_query_file,"INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('%s','%s','%7.8f');\n",
                        countries[line],countries[2*i+1], pt_amount[2*i]/pt_amount[line-1]);

                fprintf(fp_query_file,"INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('%s','%s','%7.8f');\n",
                        countries[2*i+1], countries[line], pt_amount[line-1]/pt_amount[2*i]);

            }// TODO add more formats
            i++;	
        }

        line++;
    }

    // get free memory
    free(line_buffer);
    free(query_file_name);
    fclose(p);   
    fclose(fp_query_file);


    return 0;
} 


