# PROJECT TITLE
## Exchange Rates 
Linux based C program able to scrape data from : https://www.x-rates.com
Version V1.0

## COMPANY
BSM group

### AUTHOR
Miguel Retamozo

### How do I get set up? ###

* export DEVBASE=$(pwd)
* run make
* Ask for help : ./exchange_rate -h 

### How to run
```
./exchange_rate -o <filename> -c <currency> -f <format>
```

### current time test performance
```
$time ./exchange_rate -o output.sql -c SEK -f infile

real    0m1.537s
user    0m0.054s
sys     0m0.017s

```
### -o filename
```
file.sql
```
### -c currency
AUD,USD,SEK .. etc see more at https://www.x-rates.com

### -f format:
* query:
```
INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('SEK','USD','0.12068700');
INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('USD','SEK','8.28590298');
INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('SEK','EUR','0.09895600');
INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('EUR','SEK','10.10553932');
INSERT INTO (FROM_CUR, TO_CUR, EXCHANGE_RATE) EXCHANGE_RATES VALUES('SEK','GBP','0.09013800');
```
```
mysql -u username -p database_name < file.sql
```
* infile:
```
SEK,USD,0.12068700
USD,SEK,8.28590298
SEK,EUR,0.09895600
EUR,SEK,10.10553932
SEK,GBP,0.09013800
GBP,SEK,11.09409809
```
```
LOAD DATA INFILE 'file.sql' INTO TABLE EXCHANGE_RATES FIELDS TERMINATED BY ',';
```

### Examples
```
$ ./exchange_rate -o another_file.sql -c SEK -f query
$ ./exchange_rate -o another_file.sql -c AUD -f infile
$ ./exchange_rate -o file.sql -c USD -f query

```
